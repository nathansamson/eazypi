# Eazypi

bundle exec rake release

## Installation

Install the gem and add to the application's Gemfile by executing:

    $ bundle add eazypi

If bundler is not being used to manage dependencies, install the gem by executing:

    $ gem install eazypi

## TODO

Currently this is in pre-alpha phase, and lot is left to do.

* Use components automatically
* Enforce input
  * Fix array for query parameters
* Variants of serializers
* Enforce output
* Examples into schema
* Output headers
* Security
* Tags
* Basic conformance

## Usage

### Create an API definition

Create a new API class and include `Eazypi::Api`. Then use the DSL helper methods to define the API.
Per OpenAPI spec it is required to include the info

~~~
class Api::V1::Example
  include Eazypi::Api

  info do
    title "Example API"

    version "0.0.1"
  end
end
~~~

## Define API Objects

Define API objects, they can be used with parameter objects and/or content objects in a controller

~~~
class Api::V1::Example
  include Eazypi::Serializer

  attribute :id, type: String, required: true
  attribute :name, type: String, required: true
end
~~~

More advanced attribute types are also possible

~~~
class Api::V1::Child
  include Eazypi::Serializer

  attribute :id, type: String, required: true
  attribute :name, type: String, required: true

  attribute :alternative_names, type: [String], required: false
end

class Api::V1::ExampleWithChildren
  include Eazypi::Serializer

  attribute :id, type: String, required: true
  attribute :name, type: String, required: true

  attribute :children, type: [Child], required: false
end
~~~

### Create a controller Class

Create a normal rails controller and use the DSL to define operations.

~~~
class Api::V1::ExampleController
  include include Eazypi::ApiController

  operation :get, '/examples' do
    parameter :filter do # By default a query parameter if it doesn't match a path parameter
      description "Filter examples

      schema FilterExample
    end

    response 200 do
      description "Example"

      content [API::V1::Example] # Example is defined as API Object (see above for example)
    end

    render do
      examples = ... # Examples is array of class Example NOT API::V1::Example
      respond_with examples
    end
  end

  operation :get, '/example/:id' do
    parameter :id do # Example of path parameter
      description "Id of the pet you want to find"

      schema String # Only primitive types (String, Integer, Float) are allowed in path parameters
    end

    response 200 do
      description "Example"

      content API::V1::Example # Example is defined as API Object (see above for example)
    end

    response 404 do
      description "Not found"

      content API::V1::Error
    end

    render do
      example = Example.find_by(params[:id]) # Note Example here is ActiveRecord Object, not the API view of it

      if example
        respond_with example # Will serialize example object
      else
        respond_with Error.new(message: "Not found"), status: :not_found # Will automatically serialize Error object
      end
    end
  end
end
~~~

Also for every new controller you need to define it in your Api Definition from before

~~~
class Api::V1::Example
  ...
  
  load_controller_class Api::V1::ExampleController
end
~~~

### Mount routes

In `config/routes` mount your API definition

~~~
Rails.application.routes.draw do
  scope '/api/v1' do
    Api::V1::Example.mount(self)
  end
end
~~~

### See openapi file

Run your rails server
Go to `localhost:3000/api/v1/openapi.json` or `localhost:3000/api/v1/openapi.yaml`

## Goals

The goal is to create an easy to use DSL to create nice OpenAPI defined API's.

The idea is that the code & documentation live together, but also that it is not possible to accidently change
the API but not update the documentation (especially regarding input & output parameters).


## Limitations

Feel free to prepare pull requests if you want to improve on these limitations

* Only application/json supported (not XML)
* Only supports OpenAPI 3.0.3


## Out of scope

* No UI to render the documentation. It is recommended to do this outside your rails application. For development you can use rswag-ui or similar
* Not guaranteed to deliver valid/conformant OpenAPI spec. It is recommended to run an OpenAPI validator.
  We might add errors or warnings for common mistakes, but creating a spec file without errors is not guaranteed.

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and the created tag, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/[USERNAME]/eazypi.
