# frozen_string_literal: true

module Eazypi
  # OpenaAPI spec MediaTypeObject
  class MediaType
    include SpecObject

    spec_attribute :schema

    def collect_components(schemas: nil, **kwargs)
      return unless schema

      schema.collect_components(schemas: schemas, **kwargs)
      schemas&.call(schema)
    end

    def to_openapi_spec
      {
        "schema" => schema.to_openapi_spec
      }
    end
  end
end
