# frozen_string_literal: true

module Eazypi
  # OpenAPI spec RequestBodyObject
  class RequestBody
    include SpecObject

    spec_attribute :description
    spec_attribute :required

    def initialize(&block)
      @content = {}

      super
    end

    def content(body_schema, content_type: "application/json")
      @content[content_type] = MediaType.new do
        schema Eazypi::Schema.from_object(body_schema)
      end
    end

    def collect_components(**kwargs)
      @content&.each_value do |media_type|
        media_type.collect_components(**kwargs)
      end
    end

    def to_openapi_spec
      {
        "description" => description,
        "content" => @content.transform_values(&:to_openapi_spec),
        "required" => required
      }.compact
    end
  end
end
