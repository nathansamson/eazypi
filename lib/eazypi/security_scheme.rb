# frozen_string_literal: true

module Eazypi
  # OpenAPI spec SecurityScheme
  class SecurityScheme
    include SpecObject

    spec_attribute :type
    spec_attribute :description

    spec_attribute :name # Applies only to apiKey
    spec_attribute :api_key_in # Applies only to apiKey

    spec_attribute :schema # Applies only to http
    spec_attribute :bearer_format # Applies only to http

    # spec_attribute :flows # Not supported yet
    # spec_attribute :open_id_connect_url # Not supported yet

    def to_openapi_spec
      {
        "type" => type,
        "description" => description,
        "name" => name,
        "in" => api_key_in,
        "schema" => schema,
        "bearerFormat" => bearer_format
      }.compact
    end
  end
end
