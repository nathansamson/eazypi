# frozen_string_literal: true

require "eazypi/license"

module Eazypi
  # OpenAPI spec InfoObject
  class Info
    include SpecObject

    spec_attribute :title
    spec_attribute :summary
    spec_attribute :description
    spec_attribute :terms_of_service
    spec_attribute :contact
    spec_attribute :license, License
    spec_attribute :version

    def to_openapi_spec
      {
        "title" => title,
        "summary" => summary,
        "description" => description,
        "termsOfService" => terms_of_service,
        "contact" => contact,
        "license" => license&.to_openapi_spec,
        "version" => version
      }.compact
    end
  end
end
