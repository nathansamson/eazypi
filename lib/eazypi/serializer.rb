# frozen_string_literal: true

module Eazypi
  # A serializer you can use to define your own Object types easily
  module Serializer
    extend ActiveSupport::Concern

    # Defines an Attribute on a serializer
    class Attribute
      attr_reader :name, :type, :required

      def initialize(name, type:, required:, method:, method_name:)
        @name = name
        @type = type
        @method = method
        @method_name = method_name
        @required = required
      end

      def serialize(object)
        the_value = value(object)

        Serializer.serialize_object(type, the_value)
      end

      private

      def serialize_array(the_value)
        return the_value if self.class.primitive_type?(type[0])

        the_value.map do |element_value|
          type[0].new(element_value).to_json
        end
      end

      def value(object)
        if @method
          @method.call(object)
        elsif @method_name
          object.send(@method_name)
        elsif object.is_a?(Hash)
          if object.key?(@name)
            object[@name]
          elsif object.key?(@name.to_s)
            object[@name.to_s]
          end
        else
          object.send(@name)
        end
      end
    end

    def initialize(object)
      @object = object
    end

    def to_json(*_args)
      return nil if @object.nil?

      self.class.attributes.to_h do |attribute|
        [attribute.name, attribute.serialize(@object)]
      end
    end

    included do # rubocop:disable Metrics/BlockLength
      @attributes = []

      def self.attribute(attribute_name, type:, method: nil, method_name: nil, required: false)
        @attributes << Attribute.new(
          attribute_name,
          type: type,
          method: method,
          method_name: method_name,
          required: required
        )
      end

      def self.attributes
        @attributes
      end

      def self.to_schema
        schema = Schema::Object.new(object_name)

        @attributes.each do |attribute|
          schema.property attribute.name.to_s, Schema.from_object(attribute.type, self), required: attribute.required
        end

        schema
      end

      def self.to_schema_reference
        Schema::Object.new(object_name).reference!("#/components/schemas/#{object_name}")
      end

      def self.object_name
        name.split("::").join
      end
    end

    def self.serialize_object(type_object, object) # rubocop:disable Metrics/MethodLength
      if type_object.nil?
        nil
      elsif type_object.is_a?(Array)
        object&.map do |value|
          serialize_object(type_object[0], value)
        end
      elsif [Date, Time].include?(type_object)
        object&.iso8601
      elsif [String, Integer, Float, :boolean].include?(type_object)
        object
      else
        type_object.new(object).to_json
      end
    end
  end
end
