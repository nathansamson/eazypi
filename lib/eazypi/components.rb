# frozen_string_literal: true

module Eazypi
  # OpenAPI spec ComponentsObject
  class Components
    def initialize
      @schemas = {}
      @security_schemes = {}
    end

    def add_security_scheme(name, scheme)
      @security_schemes[name] = scheme
    end

    def add_schema(name, schema)
      # Make sure it never gets a changed copy + never gets overwritten
      @schemas[name] = schema.dup unless @schemas.key?(name)

      "#/components/schemas/#{name}"
    end

    def to_openapi_spec
      {
        "schemas" => @schemas.empty? ? nil : @schemas.transform_values(&:to_openapi_spec),
        "securitySchemes" => @security_schemes.empty? ? nil : @security_schemes.transform_values(&:to_openapi_spec)
      }.compact
    end
  end
end
