# frozen_string_literal: true

module Eazypi
  # Base module for OpenAPI spec Objects
  module SpecObject
    def self.included(base)
      base.extend(ClassMethods)
    end

    def initialize(&block)
      self.load(&block) if block_given?
    end

    def load(&block)
      instance_exec(&block)
    end

    def to_openapi_spec
      raise "Not implemented"
    end

    # ClassMethods for SpecObject
    module ClassMethods
      def spec_attribute(attribute_name, spec_type = nil)
        define_method(attribute_name) do |v = nil, &block|
          if spec_type && block
            v = spec_type.new
            v.load(&block)
          end

          instance_variable_set(:"@#{attribute_name}", v) if v

          instance_variable_get(:"@#{attribute_name}")
        end
      end
    end
  end
end
