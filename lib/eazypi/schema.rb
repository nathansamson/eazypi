# frozen_string_literal: true

module Eazypi
  # JSON schema
  module Schema
    def self.from_object(object, parent = nil) # rubocop:todo Metrics/MethodLength, Metrics/CyclomaticComplexity, Metrics/PerceivedComplexity, Metrics/AbcSize
      if object == :boolean
        Schema::Primitive.new(type: "boolean")
      elsif object.is_a?(::Array)
        raise "Array needs to have one element" if object.length != 1

        Schema::Array.new(Schema.from_object(object[0], parent))
      elsif object == String
        Schema::Primitive.new(type: "string")
      elsif object == Date
        Schema::Primitive.new(type: "string", format: "date")
      elsif object == Time
        Schema::Primitive.new(type: "string", format: "date-time")
      elsif object == Integer
        Schema::Primitive.new(type: "integer")
      elsif object == Float
        Schema::Primitive.new(type: "number")
      elsif object.respond_to?(:to_schema)
        if object == parent
          object.to_schema_reference
        else
          object.to_schema
        end
      else
        raise "Can not convert #{object} to a schema"
      end
    end
  end
end

require "eazypi/schema/array"
require "eazypi/schema/object"
require "eazypi/schema/primitive"
