# frozen_string_literal: true

module Eazypi
  module Schema
    # Array schema for JSON
    class Array
      attr_reader :item_schema

      def initialize(item_schema)
        @item_schema = item_schema
      end

      def collect_components(schemas: nil, **kwargs)
        schemas&.call(@item_schema)

        @item_schema.collect_components(schemas: schemas, **kwargs)
      end

      def to_openapi_spec
        {
          "type" => "array",
          "items" => item_schema.to_openapi_spec
        }
      end

      def ==(other)
        return false unless other.is_a?(Array)

        other.instance_variable_get(:@item_schema) == @item_schema
      end
    end
  end
end
