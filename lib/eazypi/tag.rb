# frozen_string_literal: true

module Eazypi
  # OpenAPI spec Tag
  class Tag
    include SpecObject

    spec_attribute :name
    spec_attribute :description

    def to_openapi_spec
      {
        "name" => name,
        "description" => description
      }.compact
    end
  end
end
