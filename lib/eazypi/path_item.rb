# frozen_string_literal: true

module Eazypi
  # OpenAPI spec PathItemObject
  class PathItem
    include SpecObject

    spec_attribute :summary
    spec_attribute :description

    spec_attribute :get
    spec_attribute :put
    spec_attribute :post
    spec_attribute :delete
    spec_attribute :options
    spec_attribute :head
    spec_attribute :patch
    spec_attribute :trace

    def to_openapi_spec # rubocop:disable Metrics/PerceivedComplexity, Metrics/MethodLength, Metrics/CyclomaticComplexity, Metrics/AbcSize
      {
        "summary" => summary,
        "description" => description,
        "get" => get&.to_openapi_spec,
        "put" => put&.to_openapi_spec,
        "post" => post&.to_openapi_spec,
        "delete" => delete&.to_openapi_spec,
        "options" => options&.to_openapi_spec,
        "head" => head&.to_openapi_spec,
        "patch" => patch&.to_openapi_spec,
        "trace" => trace&.to_openapi_spec
      }.compact
    end
  end
end
