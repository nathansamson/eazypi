# frozen_string_literal: true

module Eazypi
  # Include this module in your Rails controller to define operations
  module ApiController
    def self.included(base)
      base.instance_variable_set(:@operations, [])
      base.include(InstanceMethods)
      base.extend(ClassMethods)
    end

    # Class methods to be used in your own API controller
    module ClassMethods
      def operation(method, path_string, &block)
        method_name = determine_method_name(method, path_string)

        op = Operation.new(self, method_name, path_string, method)
        op.load(&block)

        define_method(method_name) do
          op.call(self)
        end

        @operations << op

        op
      end

      def operations
        @operations
      end

      def determine_method_name(http_method, _path_string) # rubocop:todo Metrics/MethodLength
        controller_base_name = case http_method
                               when :get
                                 :show
                               when :post
                                 :create
                               when :patch, :put
                                 :update
                               when :delete
                                 :destroy
                               else
                                 raise "Could not generate method name"
                               end
        controller_method_name = controller_base_name

        counter = 1
        while method_defined?(controller_method_name)
          controller_method_name = "#{controller_base_name}#{counter}"
          counter += 1
        end

        controller_method_name
      end
    end

    # Instance methods to be used in your own API controller
    module InstanceMethods
      def respond_with(object, status: :ok)
        response_type = @current_operation.response_for_response_code(status)

        response_object_type = response_type.object_type_for_content_type

        render json: Serializer.serialize_object(response_object_type, object), status: status
      end
    end
  end
end
