# frozen_string_literal: true

module Eazypi
  # OpenAPI spec ResponseObject
  class Response
    include SpecObject

    spec_attribute :description

    def initialize
      @content = {}
      @object_type = {}

      super
    end

    def content(schema, content_type = "application/json")
      return if schema.nil?

      media_type = MediaType.new

      media_type.load do
        schema Schema.from_object(schema)
      end

      @content[content_type] = media_type
      @object_type[content_type] = schema
    end

    def object_type_for_content_type(content_type = "application/json")
      @object_type[content_type]
    end

    def collect_components(**kwargs)
      @content.each_value do |media_type|
        media_type.collect_components(**kwargs)
      end
    end

    def to_openapi_spec
      {
        "description" => description,
        "content" => @content.empty? ? nil : @content.transform_values(&:to_openapi_spec)
      }.compact
    end
  end
end
