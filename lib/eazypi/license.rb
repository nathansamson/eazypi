# frozen_string_literal: true

module Eazypi
  # OpenAPI spec InfoObject
  class License
    include SpecObject

    spec_attribute :name
    spec_attribute :identifier
    spec_attribute :url

    def to_openapi_spec
      {
        "name" => name,
        "identifier" => identifier,
        "url" => url
      }.compact
    end
  end
end
