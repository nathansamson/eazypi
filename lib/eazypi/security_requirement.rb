# frozen_string_literal: true

module Eazypi
  # Security Requirement OpenAPI object
  class SecurityRequirement
    def initialize(name, scopes = [])
      @name = name
      @scopes = scopes
    end

    def to_openapi_spec
      {
        @name => @scopes
      }
    end
  end
end
