# frozen_string_literal: true

module Eazypi
  # OpenAPI spec ParameterObject
  class Parameter
    include Eazypi::SpecObject

    spec_attribute :name
    spec_attribute :location
    spec_attribute :description
    spec_attribute :required
    spec_attribute :depcrecated
    spec_attribute :allow_empty_value

    spec_attribute :explode
    spec_attribute :schema

    def initialize(name:, location:, &block)
      @name = name
      @location = location

      super(&block)
    end

    def to_openapi_spec
      {
        "name" => name.to_s,
        "in" => location, # using location as in is a ruby keywork
        "description" => description,
        "required" => location == "path" ? true : required,
        "deprecated" => depcrecated,
        "allowEmptyValue" => allow_empty_value,
        "schema" => schema ? Schema.from_object(schema).to_openapi_spec : nil
      }.compact
    end
  end
end
