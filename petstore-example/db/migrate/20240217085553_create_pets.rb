# frozen_string_literal: true

class CreatePets < ActiveRecord::Migration[7.1]
  def change
    create_table :pets do |t|
      t.string :name
      t.references :category, foreign_key: { to_table: :categories }, null: true

      t.text :photo_urls
      t.integer :status, default: 0

      t.timestamps
    end
  end
end
