# frozen_string_literal: true

class CreatePetTags < ActiveRecord::Migration[7.1]
  def change
    create_table :pet_tags do |t|
      t.references :pet, foreign_key: { to_table: :pets }
      t.references :tag, foreign_key: { to_table: :tags }

      t.timestamps
    end
  end
end
