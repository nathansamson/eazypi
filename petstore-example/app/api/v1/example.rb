# frozen_string_literal: true

module Api
  module V1
    class Example
      include Eazypi::Api

      info do
        title "Example"
        description "Simple example"

        version "0.0.3"
      end
    end
  end
end
