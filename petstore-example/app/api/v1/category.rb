# frozen_string_literal: true

module Api
  module V1
    class Category
      include Eazypi::Serializer

      attribute :id, type: String, required: true
      attribute :name, type: String, required: true
    end
  end
end
