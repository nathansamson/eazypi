# frozen_string_literal: true

module Api
  module V1
    class PetStore
      include Eazypi::Api

      info do
        title "Petstore"
        description "OpenAPI implementation for a petstore"

        version "0.0.1"
      end

      load_controller Api::V1::PetsController
    end
  end
end
