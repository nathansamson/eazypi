# frozen_string_literal: true

class PetTag < ApplicationRecord
  belongs_to :pet, class_name: "Pet"
  belongs_to :tag, class_name: "Tag"
end
