# frozen_string_literal: true

require "eazypi"

RSpec.describe Eazypi::PathItem do
  it "can create a minimal spec" do
    path_item = described_class.new do
      summary "Path summary"
      description "Description"
    end

    expect(path_item.to_openapi_spec).to eq({
                                              "summary" => "Path summary",
                                              "description" => "Description"
                                            })
  end

  it "can create a spec with a few operations" do
    mock_get = instance_double(Eazypi::Operation)
    mock_put = instance_double(Eazypi::Operation)

    path_item = described_class.new do
      summary "Path summary"
      description "Description"

      get mock_get
      put mock_put
    end

    allow(mock_get).to receive(:to_openapi_spec).and_return("MOCK GET")
    allow(mock_put).to receive(:to_openapi_spec).and_return("MOCK PUT")
    expect(path_item.to_openapi_spec).to eq({
                                              "summary" => "Path summary",
                                              "description" => "Description",
                                              "get" => "MOCK GET",
                                              "put" => "MOCK PUT"
                                            })
  end
end
