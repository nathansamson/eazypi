# frozen_string_literal: true

require "eazypi"

RSpec.describe Eazypi::SecurityScheme do
  it "generates a security scheme" do
    info = described_class.new do
      type "apiKey"

      name "Authorization"
      api_key_in "header"
    end

    expect(info.to_openapi_spec).to eq({
                                         "type" => "apiKey",
                                         "name" => "Authorization",
                                         "in" => "header"
                                       })
  end
end
