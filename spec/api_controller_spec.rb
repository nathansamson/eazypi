# frozen_string_literal: true

require "eazypi"

RSpec.describe Eazypi::ApiController do
  before do
    example_controller = Class.new(ActionController::Base) do
      include Eazypi::ApiController

      operation :get, "/test" do
        description "Test"

        render do
          render json: { test: "true" }
        end
      end

      operation :get, "/test2" do
        description "Test"

        render do
          render json: { test2: "true" }
        end
      end
    end
    stub_const("ExampleController", example_controller)

    second_example_controller = Class.new(ActionController::Base) do
      include Eazypi::ApiController

      operation :post, "/test" do
        description "Test"

        render do
          render json: { test: "true" }
        end
      end
    end
    stub_const("SecondExampleController", second_example_controller)
  end

  it "can run the render" do
    controller = ExampleController.new
    allow(controller).to receive(:render)

    first_operation = ExampleController.operations[0]

    controller.send(first_operation.controller_method)
    expect(controller).to have_received(:render).with(json: { test: "true" })
  end

  it "can generate the operations" do
    expect(ExampleController.operations.length).to eq(2)

    first_operation = ExampleController.operations[0]
    expect(first_operation.path).to eq("/test")
    expect(first_operation.method).to eq(:get)

    second_operation = ExampleController.operations[1]
    expect(second_operation.path).to eq("/test2")
    expect(second_operation.method).to eq(:get)

    expect(first_operation.controller_method).not_to eq(second_operation.controller_method)
  end

  it "can generate the operations for a second class" do
    expect(SecondExampleController.operations.length).to eq(1)

    first_operation = SecondExampleController.operations[0]
    expect(first_operation.path).to eq("/test")
    expect(first_operation.method).to eq(:post)
  end
end
