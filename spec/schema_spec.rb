# frozen_string_literal: true

require "eazypi"

RSpec.describe Eazypi::Schema do
  it "can generate schema for string" do
    schema = described_class.from_object(String)

    expect(schema).to eq(Eazypi::Schema::Primitive.new(type: "string"))
  end

  it "can generate schema for boolean" do
    schema = described_class.from_object(:boolean)

    expect(schema).to eq(Eazypi::Schema::Primitive.new(type: "boolean"))
  end

  it "can generate schema for integer" do
    schema = described_class.from_object(Integer)

    expect(schema).to eq(Eazypi::Schema::Primitive.new(type: "integer"))
  end

  it "can generate schema for float" do
    schema = described_class.from_object(Float)

    expect(schema).to eq(Eazypi::Schema::Primitive.new(type: "number"))
  end

  it "can generate schema for date" do
    schema = described_class.from_object(Date)

    expect(schema).to eq(Eazypi::Schema::Primitive.new(type: "string", format: "date"))
  end

  it "can generate schema for time" do
    schema = described_class.from_object(Time)

    expect(schema).to eq(Eazypi::Schema::Primitive.new(type: "string", format: "date-time"))
  end

  it "can generate schema for a serializer" do
    schema = described_class.from_object(ExampleSerializer)

    expected_schema = Eazypi::Schema::Object.new("ExampleSerializer")
    expected_schema.property "title", Eazypi::Schema::Primitive.new(type: "string")
    expect(schema).to eq(expected_schema)
  end

  it "can generate schema for a serializer with required fields" do
    schema = described_class.from_object(ExampleWithRequiredFieldsSerializer)

    expected_schema = Eazypi::Schema::Object.new("ExampleWithRequiredFieldsSerializer")
    expected_schema.property "title", Eazypi::Schema::Primitive.new(type: "string"), required: true
    expected_schema.property "child", Eazypi::Schema::Primitive.new(type: "integer")
    expected_schema.property "numbers",
                             Eazypi::Schema::Array.new(Eazypi::Schema::Primitive.new(type: "number")),
                             required: true
    expect(schema).to eq(expected_schema)

    expect(schema.to_openapi_spec).to eq(
      {
        "type" => "object",
        "required" => %w[title numbers],
        "properties" => {
          "title" => {
            "type" => "string"
          },
          "child" => {
            "type" => "integer"
          },
          "numbers" => {
            "type" => "array",
            "items" => {
              "type" => "number"
            }
          }
        }
      }
    )
  end

  it "can generate schema for an array" do
    schema = described_class.from_object([String])

    expect(schema).to eq(Eazypi::Schema::Array.new(
                           Eazypi::Schema::Primitive.new(type: "string")
                         ))
  end

  it "can generate schema for self referencing item" do
    schema = described_class.from_object(SelfReferencingExampleSerializer)

    expected_schema = Eazypi::Schema::Object.new("SelfReferencingExampleSerializer")
    expected_schema.property "title", Eazypi::Schema::Primitive.new(type: "string")
    expected_schema.property "children", Eazypi::Schema::Array.new(
      Eazypi::Schema::Object.new("SelfReferencingExampleSerializer").reference!("SelfReferencingExampleSerializer")
    )

    expect(schema).to eq(expected_schema)
  end
end
