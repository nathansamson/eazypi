# frozen_string_literal: true

require "eazypi"
require "debug"

RSpec.configure do |config|
  # Enable flags like --only-failures and --next-failure
  config.example_status_persistence_file_path = ".rspec_status"

  # Disable RSpec exposing methods globally on `Module` and `main`
  config.disable_monkey_patching!

  config.expect_with :rspec do |c|
    c.syntax = :expect
  end
end

class ExampleSerializer
  include Eazypi::Serializer

  attribute :title, type: String
end

class ExampleWithRequiredFieldsSerializer
  include Eazypi::Serializer

  attribute :title, type: String, required: true
  attribute :child, type: Integer
  attribute :numbers, type: [Float], required: true
end

class SelfReferencingExampleSerializer
  include Eazypi::Serializer

  attribute :title, type: String
  attribute :children, type: [SelfReferencingExampleSerializer]
end
