# frozen_string_literal: true

require "eazypi"

RSpec.describe Eazypi::Info do
  it "generates a minimal spec" do
    info = described_class.new do
      title "Rspec"
      description "Rspec info"

      version "0.4.2"
    end

    expect(info.to_openapi_spec).to eq({
                                         "title" => "Rspec",
                                         "description" => "Rspec info",
                                         "version" => "0.4.2"
                                       })
  end

  it "generates a spec with license" do
    info = described_class.new do
      title "Rspec"
      description "Rspec info"

      version "0.4.2"

      license do
        name "GNU Affero General Public License v3.0 only"
        identifier "AGPL-3.0-only"
      end
    end

    expect(info.to_openapi_spec).to eq({
                                         "title" => "Rspec",
                                         "description" => "Rspec info",
                                         "version" => "0.4.2",
                                         "license" => {
                                           "name" => "GNU Affero General Public License v3.0 only",
                                           "identifier" => "AGPL-3.0-only"
                                         }
                                       })
  end
end
