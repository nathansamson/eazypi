# frozen_string_literal: true

require "eazypi"

RSpec.describe Eazypi::MediaType do
  it "generates a minimal spec" do
    mocked_schema = instance_double(Eazypi::Schema::Object)
    media_type = described_class.new do
      schema mocked_schema
    end

    allow(mocked_schema).to receive(:to_openapi_spec).and_return({ mock: "mock" })

    expect(media_type.to_openapi_spec).to eq({
                                               "schema" => { mock: "mock" }
                                             })
  end
end
