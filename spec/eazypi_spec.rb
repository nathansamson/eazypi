# frozen_string_literal: true

RSpec.describe Eazypi do
  it "has a version number" do
    expect(Eazypi::VERSION).not_to be_nil
  end
end
